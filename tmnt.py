#!/usr/bin/python3
"""RZFeeser | Alta3 Research
Exploring a simple API framework with the Python flask library
to use try:
    curl IPofServer:5055/
    curl IPofServer:5055/tmnt/
    curl IPofServer:5055/splinter/
"""

from flask import Flask
app = Flask(__name__)

@app.route("/")
def index():
    return "Cowabunga!"

@app.route("/tmnt")
def tmnt():
    return "Teenage Mutant Ninja Turtles"

@app.route("/splinter")
def splinter():
    return "Splinter taught them to be ninja team"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5055)
