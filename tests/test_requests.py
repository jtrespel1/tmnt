#!/usr/bin/python3
"""@rzfeeser
   Testing API endpoints within TMNT flask app

   Flask Testing Documentation:
   https://flask.palletsprojects.com/en/2.2.x/testing/"""

# python3 -m pip install pytest
import pytest

# The test client makes requests to the application without running a live server.
# Flask’s client extends Werkzeug’s client, see those docs for additional information.
# The client has methods that match the common HTTP request methods, such as client.get() and client.post()
def test_request_root(client):
    response = client.get("/")              # test /
    assert b"Cowabunga" in response.data

def test_request_splinter(client):
    response = client.get("/splinter")      # test /splinter
    assert b"Splinter taught them to be ninja team" in response.data
    assert "200" in response.status
