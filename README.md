# Teenage Mutant Ninja Turtles: Flask Web App

## Introduction

Cowabunga! This repository houses the Teenage Mutant Ninja Turtles (tmnt) Flask Web Application. It is suitable for testing and simple HTTP interactions (it works well as a Kubernetes test applciation). The `.gitlab-ci.yml` file ensures the container registry aways has the latest image available.  


## Getting Started

The `.gitlab-ci.yml` file in this repository will cause automation to build and commit EEs to the GitLab registry as changes are made to the code base.  

To start the flask service directly, you can run `python3 model/tmnt.py`. Alternatively, use the container found in the image repository.

## Endpoints

The following endpoints exist:  
  
**GET:**  
- `/` - Returns 200 + `Cowabunga!`
- `/tmnt` - Returns 200 + `Teenage Mutant Ninja Turtles`
- `/splinter` - Returns 200 + `Splinter taught them to be ninja team`


## Testing

Don't fear testing! This project uses `pytest` for all testing. This tool is configured by creating a `tests/` directory, in which the file `conftest.py` describes the "setup" for the application to be tested. In our case, we create an instance of our app, and then also create a client to perform some payload testing which is described in `test_requests.py`. For more on testing, see [https://flask.palletsprojects.com/en/2.2.x/tutorial/tests/#](https://flask.palletsprojects.com/en/2.2.x/tutorial/tests/#) and [https://docs.pytest.org/en/7.2.x/](https://docs.pytest.org/en/7.2.x/)


## Resources
- [Python - Flask](https://flask.palletsprojects.com/en/2.2.x/)
- [Python - PyTest](https://docs.pytest.org/)
- [GitLab - Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- [GitLab - .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab - .gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)


## Author
@RZFeeser - Author & Instructor - Feel free to reach out to if you're looking for a instructor led training solution.
